$Name$

/**
 * README for Menu Node Action module
 * @author Bob Hutchinson http://drupal.org/user/52366
 */

The Menu Node Action module creates an action that can be configured to
add a link to the node in a menu.
If the Path module is enabled it can generate an Alias automatically.
It will use the title as the link in both cases.
This is configured on a per node type basis.

Installation
============

To install the Menu Node Action module you should:

 1. Upload the menu_node_action directory into your sites/all/modules directory

 2. Enable the module under Administer -> Site building -> Modules.

 3. Setup permissions under Administer -> User management -> Permissions.

 4. Configure Menu Node Action for any node type you wish
    under Administer -> Site configuration -> Menu Node Action.

 5. Setup the action triggers under  Administer -> Site building -> Triggers
    for actions 'After saving a new post' and 'After saving an updated post'

